import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstant } from '../shared-module/route.constant';
import { LoginComponent } from '../public-module/components/login/login.component';

const routes: Routes = [
    {
        path:RouteConstant.LOGIN,
        component:LoginComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
