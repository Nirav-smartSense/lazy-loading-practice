import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../shared-module/route.constant';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.scss']
})
export class EmployeeViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get listUrl(){
    return '/' + RouteConstant.EMPLOYEE_LIST;
  }

  get detailUrl(){
    return '/' + RouteConstant.EMPLOYEE_DETAIL;
  }

}
