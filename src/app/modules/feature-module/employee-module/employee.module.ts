import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeViewComponent } from './employee-view/employee-view.component';
import { EmployeeRoutingModule } from './employee-routing.module'
import { from } from 'rxjs';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
 

@NgModule({
  declarations: [
    EmployeeListComponent,
    EmployeeViewComponent,
    EmployeeDetailComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule
  ]
})
export class EmployeeModule { 
  constructor(){
    console.log('employee module loaded');
  }
}
