import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-batch-detail',
  templateUrl: './batch-detail.component.html',
  styleUrls: ['./batch-detail.component.scss']
})
export class BatchDetailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get viewUrl(){
    return '/' + RouteConstant.BATCH_VIEW;
  }
}
