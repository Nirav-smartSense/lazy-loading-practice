import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-batch-view',
  templateUrl: './batch-view.component.html',
  styleUrls: ['./batch-view.component.scss']
})
export class BatchViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get listUrl(){
    return '/' + RouteConstant.BATCH_LIST;
  }

  get detailUrl(){
    return '/' + RouteConstant.BATCH_DETAIL;
  }
}
