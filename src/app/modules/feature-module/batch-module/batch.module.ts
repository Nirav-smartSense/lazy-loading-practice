import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BatchRoutingModule } from './batch-routing.module';
import { from } from 'rxjs';
import { BatchListComponent } from './components/batch-list/batch-list.component';
import { BatchDetailComponent } from './components/batch-detail/batch-detail.component';
import { BatchViewComponent } from './components/batch-view/batch-view.component';


@NgModule({
  declarations: [BatchListComponent, BatchDetailComponent, BatchViewComponent],
  imports: [
    CommonModule,
    BatchRoutingModule
  ]
})
export class BatchModule { 
  constructor(){
    console.log('batch module loaded');
  }
}
