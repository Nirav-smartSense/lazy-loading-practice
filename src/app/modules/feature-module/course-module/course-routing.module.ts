import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstant } from '../../shared-module/route.constant';
import { CourseListComponent } from '././components/course-list/course-list.component';
import { CourseViewComponent } from '././components/course-view/course-view.component';
import { CourseDetailComponent } from './components/course-detail/course-detail.component';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path:RouteConstant.COURSE_LIST_ROUTE,
    component:CourseListComponent
  },
  {
    path:RouteConstant.COURSE_VIEW_ROUTE,
    component:CourseViewComponent
  }
  ,
  {
    path:RouteConstant.COURSE_DETAIL_ROUTE,
    component:CourseDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule { }