import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseRoutingModule } from './course-routing.module';
import { CourseViewComponent } from './components/course-view/course-view.component';
import { CourseListComponent } from './components/course-list/course-list.component';
import { CourseDetailComponent } from './components/course-detail/course-detail.component'


@NgModule({
  declarations: [CourseViewComponent, CourseListComponent, CourseDetailComponent],
  imports: [
    CommonModule,
    CourseRoutingModule
  ]
})
export class CourseModule { 
  constructor(){
    console.log('course module loaded');
  }
}
