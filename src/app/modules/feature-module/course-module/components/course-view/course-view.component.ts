import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-course-view',
  templateUrl: './course-view.component.html',
  styleUrls: ['./course-view.component.scss']
})
export class CourseViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get listUrl(){
    return '/' + RouteConstant.COURSE_LIST;
  }

  get detailUrl(){
    return '/' + RouteConstant.COURSE_DETAIL;
  }
}
