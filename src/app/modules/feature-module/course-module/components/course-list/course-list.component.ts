import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get dashboardUrl(){
    return '/' + RouteConstant.DASHBOARD;
  }

  get viewUrl(){
    return '/' + RouteConstant.COURSE_VIEW;
  }

}
