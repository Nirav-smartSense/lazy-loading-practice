import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-notification-view',
  templateUrl: './notification-view.component.html',
  styleUrls: ['./notification-view.component.scss']
})
export class NotificationViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get detailUrl(){
    return '/' + RouteConstant.NOTIFICATION_DETAIL;
  }

  get listUrl(){
    return '/' + RouteConstant.NOTIFICATION_LIST;
  }
}
