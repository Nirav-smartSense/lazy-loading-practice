import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.scss']
})
export class NotificationDetailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get viewUrl(){
    return '/' + RouteConstant.NOTIFICATION_VIEW;
  }

}
