import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss']
})
export class NotificationListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get dashboardUrl(){
    return '/' + RouteConstant.DASHBOARD;
  }

  get viewUrl(){
    return '/' + RouteConstant.NOTIFICATION_VIEW;
  }
}
