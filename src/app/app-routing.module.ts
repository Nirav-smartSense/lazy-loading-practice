import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { RouteConstant } from './modules/shared-module/route.constant';

const routes: Routes = [
  {
    path: RouteConstant.FEATURE_MODULE_ROUTE,
    loadChildren: () =>
      import('./modules/feature-module/feature.module').then((m) => m.FeatureModule),
  },

  {
    path:'',
    pathMatch: 'full',
    redirectTo: RouteConstant.LOGIN
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
